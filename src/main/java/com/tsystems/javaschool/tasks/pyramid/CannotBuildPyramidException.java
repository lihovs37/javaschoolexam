package com.tsystems.javaschool.tasks.pyramid;

class CannotBuildPyramidException extends RuntimeException {
    CannotBuildPyramidException(String message) {
        super(message);
    }
}
